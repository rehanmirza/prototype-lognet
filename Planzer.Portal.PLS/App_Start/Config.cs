﻿using System.Configuration;


namespace Planzer.Portal.PLS
{
    public static class Config
    {
        public static string PlanzerPortalDbConnString
            => ConfigurationManager.ConnectionStrings["PlanzerPortalPlsDbConnString"].ConnectionString;

        public static string StorageConnection
            => ConfigurationManager.ConnectionStrings["StorageConnection"].ConnectionString;

        public static string FilesStorage
            => ConfigurationManager.ConnectionStrings["Files.Storage"].ConnectionString;

        public static string ImportStorage
        => ConfigurationManager.ConnectionStrings["Import.Storage"].ConnectionString;

        public static string FilesContainer
            => ConfigurationManager.AppSettings["Files.Container"];

        public static string AdressimportInstruction
          => ConfigurationManager.AppSettings["Adressimport.Instruction"];

        public static string AdressimportSampleXls
          => ConfigurationManager.AppSettings["Adressimport.SampleXls"];

        public static string AdressimportSampleXlsx
          => ConfigurationManager.AppSettings["Adressimport.SampleXlsx"];

        public static string PortalUrl
        => ConfigurationManager.AppSettings["PortalUrl"];

        public static string LogoutUrl
        => ConfigurationManager.AppSettings["LogoutUrl"];

        public static string AppAccountIdAdminClaimType
            => ConfigurationManager.AppSettings["Authorization.AppAdminAccountIdClaimType"];

        public static string AppAccountNameAdminClaimType
            => ConfigurationManager.AppSettings["Authorization.AppAdminAccountNameClaimType"];

        public static string PortalAccountChangeUrlTemplate
            => ConfigurationManager.AppSettings["Portal.AccountChangeUrlTemplate"];


    }
}