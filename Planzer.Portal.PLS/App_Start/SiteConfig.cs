﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Techgroup.AspNet.Portal.Culture;

namespace Planzer.Portal.PLS
{
    public static class SiteConfig
    {
        #region Culture

        public static readonly CultureConfig Culture = new CultureConfig(
                    supported: new ReadOnlyDictionary<string, string>(
                new Dictionary<string, string>{
                {"de-ch", "Deutsch"},
                {"fr-ch", "Français"},
                {"it-ch", "Italiano"},
                {"en-gb", "English"}
            }),
            def: "de",
            parameter: "culture");

        #endregion
    }
}