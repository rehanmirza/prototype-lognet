﻿using BundleTransformer.Core.Builders;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Transformers;
using System.Web.Optimization;

namespace Planzer.Portal.PLS
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Styles
            {
                var bundle = new StyleBundle("~/Bundles/styles.css");

                bundle.Include(
                    "~/Content/Bundles/styles.less"
                    );

                bundle.Transforms.Add(new StyleTransformer());
                bundle.Transforms.Add(new CssMinify());

                bundles.Add(bundle);
            }

            // Scripts
            {
                var bundle = new ScriptBundle("~/Bundles/scripts.js")

                    // Add References
                    .IncludeDirectory("~/Scripts/References/jQuery", "*.js", true)
                    .Include("~/Scripts/References/jQuery/mvcfoolproof.unobtrusive.js")
                    .Include("~/Scripts/References/Bootstrap/transition.js")
                    .Include("~/Scripts/References/Bootstrap/dropdown.js")
                    .Include("~/Scripts/References/Bootstrap/modal.js")
                    .Include("~/Scripts/References/Bootstrap/collapse.js")
                    //.Include("~/Scripts/References/Bootstrap/button.js")
                    .Include("~/Scripts/References/Bootstrap/tooltip.js")
                    .Include("~/Scripts/References/Bootstrap/tab.js")

                    // Main
                    .Include("~/Scripts/main.js");

                bundle.Builder = new NullBuilder();
                bundle.Transforms.Add(new ScriptTransformer());
                bundle.Transforms.Add(new JsMinify());
                bundle.Orderer = new NullOrderer();

                bundles.Add(bundle);
            }

            // Minify or not. Wenn dieser Wert nicht gesetzt ist wird im DEBUG Modus nicht Gebündelt
            // und Minifized aber im Release Modus schon. Kann auskommentiert werden wenn man im
            // Debug Modus das Minisizing und Bundeling testen möchte.
            // BundleTable.EnableOptimizations = true;
        }
    }
}