﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Techgroup.AspNet.Portal.Authentication;
using Techgroup.AspNet.Portal.Culture;
using Techgroup.AspNet.Portal.Diagnostics;

namespace Planzer.Portal.PLS
{
    public static class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new OutputCacheAttribute
            {
                NoStore = true,
                Location = OutputCacheLocation.None
            });
            filters.Add(new SetCultureAttribute());
            //filters.Add(new OnAjaxToJsonAttribute());
            filters.Add(new PortalAuthorizationAttribute());
            filters.Add(new TraceExceptionsAttribute());
            filters.Add(new HandleErrorAttribute());
        }

    }
}