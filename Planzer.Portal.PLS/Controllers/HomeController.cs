﻿using Planzer.Portal.PLS.Database;
using Planzer.Portal.PLS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Planzer.Portal.PLS.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            using (var db = new PLSDb())
            {
                var bestand = new BestandViewModel();
                bestand.PIDBestand = db.PIDBestand.ToList();
                return View(bestand);
                /*
                var lager = new LagerViewModel();
                lager.LagerListe = db.Lager.ToList();
                return View(lager);
                */
            }
        }
    }
}