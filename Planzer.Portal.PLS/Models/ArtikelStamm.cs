namespace Planzer.Portal.PLS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ArtikelStamm")]
    public partial class ArtikelStamm
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LagerNr_FK { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(24)]
        public string ArtikelNr { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(20)]
        public string MandantenNr { get; set; }

        public bool? ChargenCode { get; set; }

        public short? Rayon { get; set; }

        public byte? ArtikelArt { get; set; }

        [StringLength(20)]
        public string Lagereinheit { get; set; }
    }
}
