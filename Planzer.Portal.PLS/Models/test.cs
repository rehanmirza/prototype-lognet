namespace Planzer.Portal.PLS.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class test : DbContext
    {
        public test()
            : base("name=test")
        {
        }

        public virtual DbSet<ArtikelStamm> ArtikelStamm { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ArtikelStamm>()
                .Property(e => e.ArtikelNr)
                .IsUnicode(false);

            modelBuilder.Entity<ArtikelStamm>()
                .Property(e => e.MandantenNr)
                .IsUnicode(false);

            modelBuilder.Entity<ArtikelStamm>()
                .Property(e => e.Lagereinheit)
                .IsUnicode(false);
        }
    }
}
