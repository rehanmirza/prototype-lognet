﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planzer.Portal.PLS.Models
{
    public class Lager
    {
        public long Id { get; set; }
        public string Beschreibung { get; set; }
        public int Kapazitaet { get; set; }
        public int FilialNr { get; set; }
        public DateTimeOffset UpdateDatum { get; set; }
   
    }
}