namespace Planzer.Portal.PLS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PIDBestand")]
    public partial class PIDBestand
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LagerNr_FK { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long PID { get; set; }

        [StringLength(20)]
        public string MandantenNr { get; set; }

        [StringLength(24)]
        public string ArtikelNr_FK { get; set; }

        [StringLength(20)]
        public string ChargenNr { get; set; }

        [StringLength(30)]
        public string LieferantenChargenNr { get; set; }

        [StringLength(20)]
        public string ReservierungsCode { get; set; }

        public DateTimeOffset? EingangsDatum { get; set; }

        public DateTimeOffset? VerfallsDatum { get; set; }

        public DateTimeOffset? ProduktionsDatum { get; set; }

        public long? SSCCNr { get; set; }

        public int? TotalBestandLgE { get; set; }

        public int? ReserviertLgE { get; set; }

        public byte? SperrCodeAusgang { get; set; }

        public byte? SperrCodeCharge { get; set; }
    }
}
