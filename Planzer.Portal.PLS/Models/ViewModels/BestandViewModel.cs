﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planzer.Portal.PLS.Models
{
    public class BestandViewModel
    {
        public List<PIDBestand> PIDBestand { get; set; }
    }
}