﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planzer.Portal.PLS.Models
{
    public class LagerViewModel
    {
        public List<Lager> LagerListe { get; set; }
    }
}