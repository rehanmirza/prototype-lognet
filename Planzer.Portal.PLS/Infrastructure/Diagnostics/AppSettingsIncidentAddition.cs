﻿using System;
using System.Configuration;
using System.Linq;
using Techgroup.Diagnostics;

namespace Techgroup.AspNet.Diagnostics
{
    public static class AppSettingsIncidentAddition
    {
        /// <summary>
        /// Liest die Information für Firma, Projekt, Komponente und Komponenten Beschreibung aus
        /// den AppSettings und fügt sie hinzu.
        /// </summary>
        /// <param name="incident">The incident.</param>
        public static void Fill(Incident incident)
        {
            try
            {
                var company = ConfigurationManager.AppSettings["Incident.Company"];
                var project = ConfigurationManager.AppSettings["Incident.Project"];
                var component = ConfigurationManager.AppSettings["Incident.Component"];
                var componentDescription = ConfigurationManager.AppSettings["Incident.ComponentDescription"];

                incident
                 .AddCompanyInfo(company)
                 .AddProjectInfo(project)
                 .AddComponentInfo(component)
                 .AddComponentDescriptionInfo(componentDescription);
            }
            catch { }
        }
    }
}