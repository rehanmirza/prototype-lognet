﻿using System;
using System.Linq;
using System.Reflection;

namespace Techgroup.Diagnostics
{
    public static class AssemblyIncidentAddition
    {
        /// <summary>
        /// Ermittelt den Namen das aktuell ausgeführtem Assemblys und fügt ihn am Code hinzu.
        /// </summary>
        /// <param name="incident">The incident.</param>
        public static void Fill(Incident incident)
        {
            Fill(incident, Assembly.GetExecutingAssembly());
        }

        /// <summary>
        /// Ermittelt den Namen des Assemblys und fügt ihn am Code hinzu.
        /// </summary>
        /// <param name="incident">The incident.</param>
        /// <param name="assembly">The assembly.</param>
        public static void Fill(Incident incident, Assembly assembly)
        {
            try
            {
                if (assembly != null)
                {
                    incident
                        .AddCodeInfo("Assembly: " + assembly.ManifestModule.Name + " #" + ExtractCodeLabel());
                }
            }
            catch { }
        }

        private static string ExtractCodeLabel()
        {
            string codeLabel;

#if (DEBUG)
            codeLabel = "Debug";
#else
                codeLabel = "Release";
#endif
            return codeLabel;
        }
    }
}