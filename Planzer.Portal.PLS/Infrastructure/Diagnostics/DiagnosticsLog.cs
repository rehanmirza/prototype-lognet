﻿using System;
using System.Linq;

namespace Techgroup.Diagnostics
{
    /// <summary>
    /// Für normale Service sollte dieser Log verwendet werden. Azure Diagnostics muss im Service
    /// aktiviert sein.
    /// </summary>
    public static class DiagnosticsLog
    {
        public static void Trace(Incident incident)
        {
            switch (incident.Level)
            {
                case IncidentLevel.Error:
                    System.Diagnostics.Trace.TraceError(incident.ToString());
                    break;

                case IncidentLevel.Warning:
                    System.Diagnostics.Trace.TraceWarning(incident.ToString());
                    break;

                case IncidentLevel.Information:
                    System.Diagnostics.Trace.TraceInformation(incident.ToString());
                    break;
            }
        }
    }
}