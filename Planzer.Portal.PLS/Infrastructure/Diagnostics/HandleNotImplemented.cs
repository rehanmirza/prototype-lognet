﻿using System;
using System.Linq;
using Techgroup.AspNet.Diagnostics;

namespace Techgroup.Diagnostics
{
    public static class HandleNotImplemented
    {
        public static void DrawAttention(string message = null)
        {
#if DEBUG
            var debug = true;
#else
            var debug = false;
#endif

            if (debug)
            {
                // Beim Entwickeln und Testen wird eine Exception geworfen, so das es am meisten
                // Aufmerksamkeit auf sich zieht und schnell implementiert wird.
                throw new NotImplementedException(message);
            }
            else
            {
                // In der Produktion wird nur eine stumme Exception geworfen so kann der Benutzer
                // weiterarbeiten und wir werden dennoch informiert das etwas nicht in Ordnung ist.
                try
                {
                    throw new NotImplementedException(message);
                }
                catch (Exception ex)
                {
                    var incident = new Incident();

                    AppSettingsIncidentAddition.Fill(incident);
                    AssemblyIncidentAddition.Fill(incident);
                    ExceptionIncidentAddition.Fill(incident, ex);

                    DiagnosticsLog.Trace(incident);
                }
            }
        }
    }
}