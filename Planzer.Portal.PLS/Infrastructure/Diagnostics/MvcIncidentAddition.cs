﻿using System;
using System.Linq;
using System.Web;
using Techgroup.Diagnostics;

namespace Techgroup.AspNet.Diagnostics
{
    public static class MvcIncidentAddition
    {
        public static void Fill(Incident incident, HttpRequestBase request)
        {
            try
            {
                if (request != null)
                {
                    incident
                        .AddTriggerInfo("Aufruf: " + request.HttpMethod + " " + request.Url)
                        .AddDetailsInfo("Aufruf: " + request.HttpMethod + " " + request.Url + Environment.NewLine + request.Headers);
                }
            }
            catch { }
        }
    }
}