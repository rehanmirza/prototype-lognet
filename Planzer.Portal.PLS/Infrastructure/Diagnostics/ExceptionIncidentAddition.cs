﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Techgroup.Diagnostics
{
    public static class ExceptionIncidentAddition
    {
        /// <summary>
        /// Extrahiert die Informationen aus der Exception und fügt sie dem Code, Trigger und
        /// Details hinzu.
        /// </summary>
        /// <param name="incident">The incident.</param>
        /// <param name="ex">The ex.</param>
        public static void Fill(Incident incident, Exception ex)
        {
            try
            {
                if (ex != null)
                {
                    incident
                        .AddCodeInfo("Method: " + ExtractCode(ex))
                        .AddTriggerInfo("Exception: " + ExtractTriggerFromException(ex))
                        .AddDetailsInfo("Exception: " + ex.ToString());
                }
            }
            catch { }
        }

        private static string ExtractCode(Exception ex)
        {
            var assembly = Assembly.GetExecutingAssembly().FullName;

            var code = string.Empty;

            var stackTrace = new StackTrace(ex, true);

            foreach (var f in stackTrace.GetFrames())
            {
                var method = f.GetMethod();
                if (assembly == method.ReflectedType.Assembly.FullName)
                {
                    code = method.ReflectedType.FullName + " " + method.ToString();
                    break;
                }
            }

            return code;
        }

        private static string ExtractTriggerFromException(Exception ex)
        {
            var trigger = ex.GetType().FullName;

            var argumentException = ex as ArgumentException;

            if (argumentException != null)
            {
                return trigger + "(Parameter: " + argumentException.ParamName + ")";
            }

            var sqlEception = ex as SqlException;

            if (sqlEception != null)
            {
                return trigger + "(ErrorCode: " + +sqlEception.ErrorCode + ")";
            }

            return trigger;
        }
    }
}