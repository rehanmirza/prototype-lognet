﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Techgroup.Diagnostics
{
    public enum IncidentLevel : int
    {
        Error = 2,
        Warning = 3,
        Information = 4
    }

    public class Incident
    {
        /// <summary>
        /// Die Vorlage für die Log-Nachricht. Muss immer mit "Id: " beginnen gefolgt von einer
        /// eindeutigen Identifizierung des Fehlers (geht bis zur NewLine). Die Id wird im Status
        /// Dienst extrahiert und als Betreff im Mail verwendet. Sie Sollte so eindeutig wie nötig
        /// sein, damit gleiche oder ähnliche Fehler wiedergefunden werden können. Im Moment wird
        /// sie aus der Firma | Projekt | Komponente | Code + Trigger als Hash erstellt.
        /// </summary>
        private static string Template =
            "Id: {0}" + Environment.NewLine + Environment.NewLine +

            "Firma: {1}" + Environment.NewLine +
            "Projekt: {2}" + Environment.NewLine +
            "Ausgelöst um (ISO 8601): {3}" + Environment.NewLine + Environment.NewLine +

            "{4}" + Environment.NewLine +
            "{5}" + Environment.NewLine +

            "Code" + Environment.NewLine +
            "{6}" + Environment.NewLine +

            "Auslöser" + Environment.NewLine +
            "{7}" + Environment.NewLine +

            "Details ({8})" + Environment.NewLine +
            "{9}" + Environment.NewLine + Environment.NewLine;

        public Incident(IncidentLevel level = IncidentLevel.Error)
        {
            this.Level = level;
        }

        /// <summary>
        /// Hinweis, welche Firma betroffen ist.
        /// </summary>
        /// <value>The company.</value>
        public string Company { get; private set; }

        /// <summary>
        /// Hinweis, welches Projekt betroffen ist.
        /// </summary>
        /// <value>The project.</value>
        public string Project { get; private set; }

        /// <summary>
        /// Hinweis, welche Komponenten z.B. Azure Dienst betroffen ist.
        /// </summary>
        /// <value>The component.</value>
        public string Component { get; private set; }

        /// <summary>
        /// Beschreibung für was die betroffene Komponente gebraucht wird.
        /// </summary>
        /// <value>The component description.</value>
        public string ComponentDescription { get; private set; }

        /// <summary>
        /// Hinweis, welcher Code die Ausnahme ausgelöst hat.
        /// </summary>
        /// <value>The code.</value>
        public string Code { get; private set; }

        /// <summary>
        /// Hinweis, welcher Konstellation die Ausnahme ausgelöst hat z.B. Request URL, Parameter, etc.
        /// </summary>
        /// <value>The trigger.</value>
        public string Trigger { get; private set; }

        /// <summary>
        /// Detailierte Information zum Ereignis.
        /// </summary>
        /// <value>The details.</value>
        public string Details { get; private set; }

        public IncidentLevel Level { get; set; }

        public Incident AddProjectInfo(string info)
        {
            this.Project += Validate(info, " ");

            return this;
        }

        public Incident AddComponentInfo(string info)
        {
            this.Component += Validate(info, " ");

            return this;
        }

        public Incident AddComponentDescriptionInfo(string info)
        {
            this.ComponentDescription += Validate(info, Environment.NewLine);

            return this;
        }

        public Incident AddCodeInfo(string info)
        {
            this.Code += Validate(info, Environment.NewLine);

            return this;
        }

        public Incident AddTriggerInfo(string info)
        {
            this.Trigger += Validate(info, Environment.NewLine);

            return this;
        }

        public Incident AddDetailsInfo(string info)
        {
            this.Details += Validate(info, Environment.NewLine);

            return this;
        }

        public Incident AddCompanyInfo(string info)
        {
            this.Company += Validate(info, " ");

            return this;
        }

        public override string ToString()
        {
            var id = this.Company + "| " + this.Project + "| " + this.Component + "| " + CreateHash(this.Code + this.Trigger);
            var detailsHash = CreateHash(this.Details);
            var now = DateTimeOffset.Now.ToString("yyyy-MM-ddTHH:mm:sszzz", CultureInfo.InvariantCulture);

            return string.Format(Template, id, this.Company, this.Project, now, this.Component, this.ComponentDescription, this.Code, this.Trigger, detailsHash, this.Details);
        }

        /// <summary>
        /// Create a hash. Visit: http://stackoverflow.com/questions/3984138/hash-string-in-c-sharp/6839784#6839784
        /// </summary>
        /// <param name="s">The input string.</param>
        /// <returns></returns>
        private static string CreateHash(string s)
        {
            var sb = new StringBuilder();
            var algorithm = MD5.Create();
            var hash = algorithm.ComputeHash(Encoding.UTF8.GetBytes(s));

            foreach (byte b in hash)
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        private string Validate(string info, string seperator)
        {
            if (string.IsNullOrWhiteSpace(info))
                return string.Empty;

            info = info.Trim(seperator.ToCharArray());

            if (string.IsNullOrWhiteSpace(info))
                return string.Empty;

            return info + seperator;
        }
    }
}