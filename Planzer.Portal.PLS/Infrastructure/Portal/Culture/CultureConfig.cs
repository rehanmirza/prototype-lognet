﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Techgroup.AspNet.Portal.Culture
{
    public class CultureConfig
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CultureConfig" /> class.
        /// </summary>
        /// <param name="supported">
        /// The supported culture / languages from the site. Alle müssen klein geschrieben sein und
        /// von ISO 639-1 damit sie auch im html[lang] funktionieren.
        /// Source: http://www.w3schools.com/tags/ref_language_codes.asp
        /// </param>
        /// <param name="def">The default culture.</param>
        /// <param name="parameter">The name of the parameter eg. Cookiename.</param>
        public CultureConfig(IReadOnlyDictionary<string, string> supported, string def, string parameter)
        {
            this.Supported = supported;
            this.Default = def;
            this.Parameter = parameter;
        }

        public string Current
        {
            get
            {
                var culture = CurrentInfo.Name.ToLower(CultureInfo.InvariantCulture);
                return NormalizeCulture(culture);
            }
        }

        public CultureInfo CurrentInfo => Thread.CurrentThread.CurrentUICulture;

        public string Default { get; }

        public string Parameter { get; }

        /// <summary>
        /// The supported culture / languages from the site. Alle sind klein geschrieben und von ISO
        /// 639-1 damit sie auch im html[lang] funktionieren.
        /// Source: http://www.w3schools.com/tags/ref_language_codes.asp
        /// </summary>
        public IReadOnlyDictionary<string, string> Supported { get; }

        /// <summary>
        /// Gets the current culture from 1. from the user Claim 2. Urls QueryString 3. from the
        /// cookie 4. browser culture (an information from the browser that the user can set) 5.
        /// default value.
        /// </summary>
        /// <param name="context">The context from the request.</param>
        /// <returns>The culture code.</returns>
        public string From(ControllerContext context)
        {
            var currentCulture = FromClaim();
            if (!string.IsNullOrEmpty(currentCulture))
                return currentCulture;

            currentCulture = FromQueryString(context);
            if (!string.IsNullOrEmpty(currentCulture))
                return currentCulture;

            currentCulture = FromCookie(context);
            if (!string.IsNullOrEmpty(currentCulture))
                return currentCulture;

            currentCulture = FromBrowser(context);
            if (!string.IsNullOrEmpty(currentCulture))
                return currentCulture;

            return this.Default;
        }

        /// <summary>
        /// Determines whether the specified culture is contained in the supported list. Folgende
        /// Kombination sind valid: de-ch =&gt; de, de =&gt; de-ch, de-de =&gt; de-ch
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        public bool IsValid(string culture)
                => !string.IsNullOrEmpty(culture) &&
                    this.Supported.Keys.Any(s => string.Equals(s, culture, StringComparison.InvariantCultureIgnoreCase) || string.Equals(s.Split('-').First(), culture.Split('-').First(), StringComparison.InvariantCultureIgnoreCase));

        /// <summary>
        /// Sucht die beste Übereinstimmung und gibt diese zurück.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        public string NormalizeCulture(string culture)
        {
            if (string.IsNullOrEmpty(culture))
                return this.Default;

            foreach (var s in this.Supported.Keys)
            {
                if (string.Equals(s, culture, StringComparison.InvariantCultureIgnoreCase))
                    return s; // Eine exakte Übereinstimmung gefunden
            }

            foreach (var s in this.Supported.Keys)
            {
                if (string.Equals(s.Split('-').First(), culture.Split('-').First(), StringComparison.InvariantCultureIgnoreCase))
                    return s;// Eine Übereinstimmung (z.B. de-ch => de, de => de-ch, de-de => de-ch) gefunden
            }

            return this.Default;
        }

        public void Save(ControllerContext context, string cultureCode)
        {
            SaveInClaim(cultureCode);
            SaveInCookie(context, cultureCode);
            SaveInThread(cultureCode);
        }

        /// <summary>
        /// Extract the culture from the current Authentification Claim.
        /// </summary>
        private string FromClaim()
        {
            var claimCulture = ClaimsPrincipal.Current.FindFirst(ClaimTypes.UserData);

            if (claimCulture != null && IsValid(claimCulture.Value))
            {
                return claimCulture.Value;
            }

            return string.Empty;
        }

        /// <summary>
        /// Extract the culture from the QueryString part from the url.
        /// </summary>
        /// <param name="context">The filter context from the request.</param>
        /// <returns>
        /// The culture code from the QueryString part from the url or an empty string if nothing can found.
        /// </returns>
        private string FromQueryString(ControllerContext context)
        {
            var queryCulture = context.RequestContext.HttpContext.Request.QueryString[this.Parameter] as string;

            if (IsValid(queryCulture))
            {
                return queryCulture;
            }

            return string.Empty;
        }

        /// <summary>
        /// Extract the culture from the cookies.
        /// </summary>
        /// <param name="context">The filter context from the request.</param>
        /// <returns>The culture code from the cookies or an empty string if nothing can found.</returns>
        private string FromCookie(ControllerContext context)
        {
            var userCookie = context.RequestContext.HttpContext.Request.Cookies[this.Parameter];

            if (userCookie != null && IsValid(userCookie.Value))
            {
                return userCookie.Value;
            }

            return string.Empty;
        }

        /// <summary>
        /// Extract the culture from the browser settings.
        /// </summary>
        /// <param name="context">The context from the request.</param>
        /// <returns>
        /// The culture code from the browser settings or an empty string if nothing can found.
        /// </returns>
        private string FromBrowser(ControllerContext context)
        {
            var browserLanguages = context.RequestContext.HttpContext.Request.UserLanguages;

            if (browserLanguages != null && browserLanguages.Any())
            {
                foreach (var thisBrowserLanguage in browserLanguages.Select(l => l.Split(';').FirstOrDefault()))
                {
                    if (IsValid(thisBrowserLanguage))
                    {
                        return thisBrowserLanguage;
                    }
                }
            }
            return string.Empty;
        }

        private void SaveInClaim(string cultureCode)
        {
            if (ClaimsPrincipal.Current.FindFirst(ClaimTypes.UserData).Value == cultureCode)
                return;

            var sam = FederatedAuthentication.SessionAuthenticationModule;

            SessionSecurityToken token;

            if (sam.TryReadSessionTokenFromCookie(out token))
            {
                var identity = token.ClaimsPrincipal.Identities.First();

                // Entferne existierende Claims falls vorhanden
                var claims = ClaimsPrincipal.Current.FindAll(x => x.Type == ClaimTypes.UserData);

                foreach (var c in claims)
                    identity.RemoveClaim(c);

                // Den neuen Claim hinzufügen
                var newClaim = new Claim(ClaimTypes.UserData, cultureCode);
                identity.AddClaim(newClaim);

                // Generiere das SecurityToken neu und überschreibe das cookie mit dem neuen Token
                var newToken = sam.CreateSessionSecurityToken(token.ClaimsPrincipal, token.Context, token.ValidFrom, token.ValidTo, token.IsPersistent);
                sam.WriteSessionTokenToCookie(newToken);
            }
        }

        /// <summary>
        /// Saves the culture in the cookies if it not.
        /// </summary>
        /// <param name="context">The context from the request.</param>
        /// <param name="cultureCode">The culture code to save.</param>
        private void SaveInCookie(ControllerContext context, string cultureCode)
        {
            var cookieValue = FromCookie(context);

            //Save only in the cookie if not set or the value changed.
            if (string.IsNullOrEmpty(cookieValue) || !string.Equals(cookieValue, cultureCode, StringComparison.InvariantCultureIgnoreCase))
            {
                HttpContext.Current.Response.Cookies.Set(
                    new HttpCookie(this.Parameter, cultureCode)
                    {
                        HttpOnly = true,
                        Expires = DateTime.Now.AddDays(14)
                    }
                );
            }
        }

        /// <summary>
        /// Saves the culture in the thread so that the application runs in the right culture.
        /// </summary>
        /// <param name="cultureCode">The culture code to save.</param>
        private void SaveInThread(string cultureCode)
        {
            var culture = new CultureInfo(cultureCode);

            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
        }
    }
}