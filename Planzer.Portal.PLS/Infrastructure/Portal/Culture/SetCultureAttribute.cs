﻿using Planzer.Portal.PLS;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Techgroup.AspNet.Portal.Culture
{
    /// <summary>
    /// This attribute handled the culture.
    /// </summary>
    public class SetCultureAttribute : FilterAttribute, IActionFilter
    {
        /// <summary>
        /// Called after the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context from the request.</param>
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }

        /// <summary>
        /// Called before an action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context from the request.</param>
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var cultureCode = SiteConfig.Culture.From(filterContext);

            cultureCode = SiteConfig.Culture.NormalizeCulture(cultureCode);

            SiteConfig.Culture.Save(filterContext, cultureCode);
        }
    }
}