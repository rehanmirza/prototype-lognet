﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Techgroup.AspNet.Portal.Authentication
{
    public static class SystemRoles
    {
        /// <summary>
        /// System Admin, kann Accounts verwalten
        /// </summary>
        public const string PORT_SYS_ADM = "PortSysAdm";

        /// <summary>
        /// Admin, kann Apps und Benutzer verwalten
        /// </summary>
        public const string PORT_ADM = "PortAdm";

        /// <summary>
        /// App Admin, kann Kacheln verwalten
        /// </summary>
        public const string PORT_APP_ADM = "PortAppAdm";

        /// <summary>
        /// Admin beim Kunden, kann seine Benutzer verwalten
        /// </summary>
        public const string PORT_KUND_ADM = "PortKundAdm";

        /// <summary>
        /// Benutzer beim Kunden
        /// </summary>
        public const string PORT_USER = "PortUser";

        // Für AuthorisierungAttribut z.B. [Authorisierung(Roles = SystemRoles.ALL_ADMINS_ROLES)]
        public const string SYSADM_AND_PORTADM_ROLES = PORT_SYS_ADM + "," + PORT_ADM;

        public const string PLANZER_ADMINS_ROLES = PORT_SYS_ADM + "," + PORT_ADM + "," + PORT_APP_ADM;
        public const string ALL_ADMINS_ROLES = PORT_SYS_ADM + "," + PORT_ADM + "," + PORT_APP_ADM + "," + PORT_KUND_ADM;

        // Ranking
        public const int PORT_SYS_ADM_RANK = 0;

        public const int PORT_ADM_RANK = 1;
        public const int PORT_APP_ADM_RANK = 2;
        public const int PORT_KUND_ADM_RANK = 3;
        public const int PORT_USER_RANK = 4;
        public static readonly string[] SYSADM_AND_PORTADM = { PORT_SYS_ADM, PORT_ADM };
        public static readonly string[] PLANZER_ADMINS = { PORT_SYS_ADM, PORT_ADM, PORT_APP_ADM };
        public static readonly string[] ALL_ADMINS = { PORT_SYS_ADM, PORT_ADM, PORT_APP_ADM, PORT_KUND_ADM };

        public static readonly Dictionary<string, int> Ranks = new Dictionary<string, int>
        {
          { PORT_SYS_ADM, PORT_SYS_ADM_RANK },
          { PORT_ADM, PORT_ADM_RANK },
          { PORT_APP_ADM, PORT_APP_ADM_RANK },
          { PORT_KUND_ADM, PORT_KUND_ADM_RANK },
          { PORT_USER, PORT_USER_RANK }
        };

        public static bool HasPermissionFor(string role, string roleToAskPermissionFor)
        {
            var roleRank = Ranks[role];
            var permissionForRank = Ranks[roleToAskPermissionFor];

            // Wenn role kleiner ist als roleToAskPermissionFor, dann ist currentRole eine höhrere
            // Rolle als roleToAskPermissionFor und somit berechtigt.
            return roleRank < permissionForRank;
        }
    }
}