﻿using Planzer.Portal.PLS;
using System;
using System.Configuration;
using System.IdentityModel.Services;
using System.IdentityModel.Services.Configuration;
using System.Security.Claims;
using System.Web;

namespace Techgroup.AspNet.Portal.Authentication
{
    public static class PortalAuthenticationCookie
    {
        public const double LifeTimeInMinutes = 90;

        /// <summary>
        /// Sets the configuration for cookie handler. Muss in der Global.asax.cs in der Methode
        /// MvcApplication.Application_Start als Event registriert werden.
        /// FederatedAuthentication.FederationConfigurationCreated += PortalAuthenticationCookie.SetConfigForCookieHandler;
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">
        /// The <see cref="FederationConfigurationCreatedEventArgs" /> instance containing the event data.
        /// </param>
        public static void SetConfigForCookieHandler(object sender, FederationConfigurationCreatedEventArgs e)
        {
            e.FederationConfiguration.CookieHandler.Name = ConfigurationManager.AppSettings["Authorization.Cookie"];
            e.FederationConfiguration.CookieHandler.Domain = ConfigurationManager.AppSettings["Authorization.Domain"];
        }

        /// <summary>
        /// Refreshes the security token. Muss in der Global.asax.cs in der Methode
        /// MvcApplication.SessionAuthenticationModule_SessionSecurityTokenReceived aufgerufen werden.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">
        /// The <see cref="SessionSecurityTokenReceivedEventArgs" /> instance containing the event data.
        /// </param>
        public static void RefreshSecurityToken(object sender, SessionSecurityTokenReceivedEventArgs e)
        {
            var now = DateTime.UtcNow;
            var validFrom = e.SessionToken.ValidFrom;
            var validTo = e.SessionToken.ValidTo;

            var isNotExpired = now < validTo;
            var half = validFrom.AddMinutes(LifeTimeInMinutes / 2d);
            var isOverHalftime = now > half;

            if (isNotExpired && isOverHalftime)
            {
                var sam = sender as SessionAuthenticationModule;
                e.SessionToken = sam.CreateSessionSecurityToken(e.SessionToken.ClaimsPrincipal, e.SessionToken.Context,
                now, now.AddMinutes(LifeTimeInMinutes), e.SessionToken.IsPersistent);
                e.ReissueCookie = true;
            }
        }

        /// <summary>
        /// Diese Funktion wird nur für das debuggen im localhost benutzt, da aufgrund des
        /// Domänen-Logins sonst lokal nicht getestet werden kann. Muss in der Global.asax.cs in der
        /// Methode MvcApplication.Application_BeginRequest aufgerufen werden.
        /// </summary>
        /// <param name="Context">The context.</param>
        public static void SetLocalCookieForDebugWithLocalhost(HttpContext Context)
        {
#if (DEBUG)
            var debug = true;
#else
         var debug = false;
#endif

            // Im Localhost ein statisches cookie setzen sodass debugged werden kann ohne Anmeldung.
            if (debug && Context.Request.IsLocal)
            {
                var identity = new ClaimsIdentity("Planzer.Portal");

                // NameIdentifier für den AntiforgeryToken (siehe Global.asax.cs) und Claim Identifikation
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, "1"));

                identity.AddClaim(new Claim(ClaimTypes.Name, "speter@techgroup.ch"));
                identity.AddClaim(new Claim(ClaimTypes.UserData, "de-ch"));
                identity.AddClaim(new Claim(ClaimTypes.Role, PortalRoles.PortSysAdm));
                identity.AddClaim(new Claim(PortalClaimTypes.AcccountId, "3"));
                identity.AddClaim(new Claim(PortalClaimTypes.Apps, PortalAuthorizationAttribute.AppSteuercode));
                identity.AddClaim(new Claim(PortalClaimTypes.PortalAccountName, "Techgroup Schweiz AG"));
                if (!string.IsNullOrEmpty(Config.AppAccountIdAdminClaimType))
                {
                    identity.AddClaim(new Claim(Config.AppAccountIdAdminClaimType, "3"));
                }
                var principal = new ClaimsPrincipal(identity);

                //Cookie erstellen
                var sam = FederatedAuthentication.SessionAuthenticationModule;
                sam.CookieHandler.RequireSsl = false;
                sam.CookieHandler.PersistentSessionLifetime = TimeSpan.FromMinutes(LifeTimeInMinutes);
                sam.WriteSessionTokenToCookie(sam.CreateSessionSecurityToken(principal, string.Empty, DateTime.UtcNow, DateTime.UtcNow.AddMinutes(LifeTimeInMinutes), false));
            }
        }
    }
}