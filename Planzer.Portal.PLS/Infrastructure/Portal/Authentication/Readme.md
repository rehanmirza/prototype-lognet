﻿Portal Authentication
=====================
Füge die Referenzen zu den Assemblies System.IdentityModel und System.IdentityModel.Services hinzu.

Integriere die Konfigurationen aus der ./Web.config in die Web.config des Projektes.

Kopiere Klassen in diesem Ordner in dein Projekt und befolge die anweisungen im PortalAuthenticationCookie für die Entsprechenden Funktion und füge die "Authorization.*" Einstellungen der AppSettings hinzu (PortalAuthorizationAttribute, PortalAutheticationCookie, PortalClaimTypes)

Danach kann auf die Informationen aus dem SessionTokenCookie zugegriffen werden.

    // z.B. via AuthorizeAttribute
    [Authorize]
    public ActionResult ActionName()
    {

    // oder nur bestimmte Rollen
    [Authorize(Roles = "Admin,User")]
    public ActionResult ActionName()
    {

    // oder in einer Action
    User.Identity.Name;
    User.Identity.IsAuthenticated;
    User.IsInRole("Admin");

    ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value; // Id
    ClaimsPrincipal.Current.FindFirst("...").Value;