﻿using System;
using System.Linq;
using System.Security.Principal;

namespace Techgroup.AspNet.Portal.Authentication
{
    public static class IPrincipalExtension
    {
        public static bool IsInAnyRole(this IPrincipal principal, params string[] roles)
            => roles.Any(principal.IsInRole);
    }
}