﻿using Planzer.Portal.PLS;
using System.Security.Claims;

namespace Techgroup.AspNet.Portal.Authentication
{
    public static class PortalClaims
    {
        public static long GetUserId()
            => long.Parse(GetValue(ClaimTypes.NameIdentifier));

        public static string GetUserName()
            => GetValue(ClaimTypes.Name);

        public static long GetAccountId()
        {
            if (ClaimsPrincipal.Current.IsInAnyRole(PortalRoles.PlanzerAdmins) && !string.IsNullOrEmpty(Config.AppAccountIdAdminClaimType))
            {
                if (ClaimsPrincipal.Current.HasClaim(x => x.Type == Config.AppAccountIdAdminClaimType))
                {
                    return long.Parse(GetValue(Config.AppAccountIdAdminClaimType));
                }
            }

            return long.Parse(GetValue(PortalClaimTypes.AcccountId));
        }

        public static string GetAdminAccountNameIfAvailable()
        {
            if (ClaimsPrincipal.Current.IsInAnyRole(PortalRoles.PlanzerAdmins))
            {
                if (ClaimsPrincipal.Current.HasClaim(x => x.Type == Config.AppAccountNameAdminClaimType))
                {
                    return GetValue(Config.AppAccountNameAdminClaimType);
                }
            }

            return "";
        }

        public static string GetAdminAccountNameInBrackets()
        {
            var accountName = GetAdminAccountNameIfAvailable();

            if (string.IsNullOrEmpty(accountName))
                return "";

            return "(" + accountName + ")";
        }

        public static string GetCurrentAccountName()
        {
            if (ClaimsPrincipal.Current.IsInAnyRole(PortalRoles.PlanzerAdmins))
            {
                if (ClaimsPrincipal.Current.HasClaim(x => x.Type == Config.AppAccountNameAdminClaimType))
                {
                    return GetValue(Config.AppAccountNameAdminClaimType);
                }
            }

            return GetValue(PortalClaimTypes.PortalAccountName);
        }

        public static string GetAccountName()
        {
            return GetValue(PortalClaimTypes.PortalAccountName);
        }

        public static string GetValue(string claimType)
            => ClaimsPrincipal.Current.FindFirst(claimType).Value;
    }
}