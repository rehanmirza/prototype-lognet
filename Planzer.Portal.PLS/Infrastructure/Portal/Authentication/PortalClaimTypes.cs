﻿namespace Techgroup.AspNet.Portal.Authentication
{
    public static class PortalClaimTypes
    {
        public const string Apps = "PortalPermissionedApps";

        public const string AcccountId = "PortalAccountId";
        public const string PortalAccountName = "PortalAccountName";

        public const string Brand = "Brand";
    }
}