﻿using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Techgroup.AspNet.Portal.Authentication
{
    public sealed class PortalAuthorizationAttribute : AuthorizeAttribute
    {
        public PortalAuthorizationAttribute(bool checkForAppAuthorization = true)
        {
            CheckForAppAuthorization = checkForAppAuthorization;
        }

        public bool CheckForAppAuthorization { get; set; }

        public static string LoginUrl
            => ConfigurationManager.AppSettings["Authorization.LoginUrl"];

        public static string AppSteuercode
            => ConfigurationManager.AppSettings["Authorization.AppSteuercode"];

        public static bool IsAuthorizedForThisApp()
        {
            if (!ClaimsPrincipal.Current.HasClaim(x => x.Type == PortalClaimTypes.Apps))
                return false;

            var appCodes = ClaimsPrincipal.Current.FindFirst(PortalClaimTypes.Apps).Value;

            if (string.IsNullOrEmpty(appCodes))
                return false;

            return appCodes.Split(';').Contains(AppSteuercode);
        }

        public override void OnAuthorization(System.Web.Mvc.AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            if (CheckForAppAuthorization && ClaimsPrincipal.Current.Identity.IsAuthenticated && !IsAuthorizedForThisApp())
            {
                filterContext.HttpContext.Response.StatusCode = 302;
                filterContext.Result = new RedirectResult("/notauthorized");
            }
        }

        protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            filterContext.HttpContext.Response.StatusCode = 401;
            var loginUrl = LoginUrl;

            loginUrl = loginUrl.TrimEnd('/');

            loginUrl += "?returnUrl=" + HttpUtility.UrlEncode(filterContext.HttpContext.Request.Url.AbsoluteUri);

            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.Result = new JsonResult
                {
                    Data = new
                    {
                        Error = "NotAuthorized",
                        LogOnUrl = loginUrl
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                filterContext.Result = new RedirectResult(loginUrl);
            }
        }
    }
}