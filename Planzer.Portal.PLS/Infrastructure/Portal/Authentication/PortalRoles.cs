﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Techgroup.AspNet.Portal.Authentication
{
    public static class PortalRoles
    {
        /// <summary>
        /// System Admin, kann Accounts verwalten
        /// </summary>
        public const string PortSysAdm = "PortSysAdm";

        /// <summary>
        /// Admin, kann Apps und Benutzer verwalten
        /// </summary>
        public const string PortAdm = "PortAdm";

        /// <summary>
        /// App Admin, kann Kacheln verwalten
        /// </summary>
        public const string PortAppAdm = "PortAppAdm";

        /// <summary>
        /// Admin beim Kunden, kann seine Benutzer verwalten
        /// </summary>
        public const string PortKundAdm = "PortKundAdm";

        /// <summary>
        /// Benutzer beim Kunden
        /// </summary>
        public const string PortUser = "PortUser";

        // Für AuthorisierungAttribut z.B. [Authorisierung(Roles = SystemRoles.ALL_ADMINS_ROLES)]
        public const string SysAdmAndPortAdmRoles = PortSysAdm + "," + PortAdm;

        public const string PlanzerAdminsRoles = PortSysAdm + "," + PortAdm + "," + PortAppAdm;
        public const string AllAdminsRoles = PortSysAdm + "," + PortAdm + "," + PortAppAdm + "," + PortKundAdm;

        // Ranking
        public const int PortSysAdmRank = 0;

        public const int PortAdmRank = 1;
        public const int PortAppAdmRank = 2;
        public const int PortKundAdmRank = 3;
        public const int PortUserRank = 4;
        public static readonly string[] SysAdmAndPortAdm = { PortSysAdm, PortAdm };
        public static readonly string[] PlanzerAdmins = { PortSysAdm, PortAdm, PortAppAdm };
        public static readonly string[] AllAdmins = { PortSysAdm, PortAdm, PortAppAdm, PortKundAdm };

        public static readonly Dictionary<string, int> Ranks = new Dictionary<string, int>
        {
          { PortSysAdm, PortSysAdmRank },
          { PortAdm, PortAdmRank },
          { PortAppAdm, PortAppAdmRank },
          { PortKundAdm, PortKundAdmRank },
          { PortUser, PortUserRank }
        };

        public static bool HasPermissionFor(string role, string roleToAskPermissionFor)
        {
            var roleRank = Ranks[role];
            var permissionForRank = Ranks[roleToAskPermissionFor];

            // Wenn role kleiner ist als roleToAskPermissionFor, dann ist currentRole eine höhrere
            // Rolle als roleToAskPermissionFor und somit berechtigt.
            return roleRank < permissionForRank;
        }
    }
}