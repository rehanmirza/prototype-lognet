﻿using System;
using System.Linq;

namespace Techgroup.AspNet.Portal.Brand
{
    public class BrandInfo
    {
        public string Code { get; set; }
        public string PathToLogo { get; set; }
    }
}