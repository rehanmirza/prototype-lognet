﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Techgroup.AspNet.Portal.Authentication;
using Techgroup.Diagnostics;

namespace Techgroup.AspNet.Portal.Brand
{
    public static class BrandConfig
    {
        static BrandConfig()
        {
            try
            {
                var supportedBrandsSettings = ConfigurationManager.AppSettings["Brand.Supported"];

                if (string.IsNullOrWhiteSpace(supportedBrandsSettings))
                {
                    HandleNotImplemented.DrawAttention("Die AppSetting Brand.Supported fehlt. Die AppSetting Brand.Supported erwartet eine mit ; separierte Liste von unterstützen Brands. Der erste Eintrag wird als Default verwendet.");
                }

                var supportedBrands = supportedBrandsSettings.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                if (supportedBrands.Count() < 2)
                {
                    HandleNotImplemented.DrawAttention("Die AppSetting Brand.Supported erwartet eine mit ; separierte Liste von unterstützen Brands. Der erste Eintrag wird als Default verwendet.");
                }

                Supported = supportedBrands.ToList().AsReadOnly();

                Default = Supported.First();
            }
            catch (Exception ex)
            {
                HandleNotImplemented.DrawAttention("Die BrandConfig hat einen Fehler verursacht. Die AppSetting Brand.Supported erwartet eine mit ; separierte Liste von unterstützen Brands. Der erste Eintrag wird als Default verwendet. " + ex);
            }
        }

        public static string Current { get; private set; }

        public static BrandInfo CurrentInfo
        {
            get
            {
                var brandInfo = new BrandInfo { Code = Current };

                try
                {
                    var pathToLogos = ConfigurationManager.AppSettings["Brand.PathToLogo"];

                    if (string.IsNullOrWhiteSpace(pathToLogos))
                    {
                        HandleNotImplemented.DrawAttention("Die AppSetting Brand.PathToLogo fehlt. Die AppSetting Portal.Brand.PathToLogo erwartet einen URL mit einem {0} Platzhalter für den Brand.");
                    }

                    brandInfo.PathToLogo = string.Format(CultureInfo.InvariantCulture, pathToLogos, Current.ToLowerInvariant());
                }
                catch (Exception ex)
                {
                    HandleNotImplemented.DrawAttention("Die BrandConfig hat einen Fehler verursacht. Die AppSetting Brand.PathToLogo erwartet einen URL mit einem {0} Platzhalter für den Brand. " + ex);
                }

                return brandInfo;
            }
        }

        public static string Default { get; }

        public static string Parameter { get; } = "brand";

        public static IReadOnlyList<string> Supported { get; }

        /// <summary>
        /// Gets the current brand from 1. Urls QueryString 2. the user Claim 3. the cookie 5.
        /// default value.
        /// </summary>
        /// <param name="context">The context from the request.</param>
        /// <returns>The brand code.</returns>
        public static string From(ControllerContext context)
        {
            var currentBrand = FromQueryString(context);
            if (!string.IsNullOrEmpty(currentBrand))
                return currentBrand;

            currentBrand = FromClaim();
            if (!string.IsNullOrEmpty(currentBrand))
                return currentBrand;

            currentBrand = FromCookie(context);
            if (!string.IsNullOrEmpty(currentBrand))
                return currentBrand;

            return Default;
        }

        /// <summary>
        /// Determines whether the specified brand is contained in the supported list.
        /// </summary>
        /// <param name="brand">The brand.</param>
        /// <returns></returns>
        public static bool IsValid(string brand)
                => !string.IsNullOrEmpty(brand) &&
                    Supported.Any(s => string.Equals(s, brand, StringComparison.InvariantCultureIgnoreCase));

        /// <summary>
        /// Sucht die beste Übereinstimmung und gibt diese zurück.
        /// </summary>
        /// <param name="brand">The brand.</param>
        /// <returns></returns>
        public static string NormalizeBrand(string brand)
        {
            if (string.IsNullOrEmpty(brand))
                return Default;

            foreach (var s in Supported)
            {
                if (string.Equals(s, brand, StringComparison.InvariantCultureIgnoreCase))
                    return s; // Eine exakte Übereinstimmung gefunden
            }

            foreach (var s in Supported)
            {
                if (string.Equals(s.Replace("-", string.Empty), brand.Replace("-", string.Empty), StringComparison.InvariantCultureIgnoreCase))
                    return s;// Eine Übereinstimmung (z.B. quali-night => qualinight) gefunden
            }

            return Default;
        }

        public static void Save(ControllerContext context, string brandCode)
        {
            SaveInClaim(brandCode);
            SaveInCookie(context, brandCode);
            SaveInViewBag(context, brandCode);
            SaveInThread(brandCode);
        }

        /// <summary>
        /// Extract the brand from the current Authentication Claim.
        /// </summary>
        private static string FromClaim()
        {
            var claimBrand = ClaimsPrincipal.Current.FindFirst(PortalClaimTypes.Brand);

            if (claimBrand != null && IsValid(claimBrand.Value))
            {
                return claimBrand.Value;
            }

            return string.Empty;
        }

        /// <summary>
        /// Extract the brand from the QueryString part from the url.
        /// </summary>
        /// <param name="context">The filter context from the request.</param>
        /// <returns>
        /// The brand code from the QueryString part from the url or an empty string if nothing can found.
        /// </returns>
        private static string FromQueryString(ControllerContext context)
        {
            var queryBrand = context.RequestContext.HttpContext.Request.QueryString[Parameter] as string;

            if (IsValid(queryBrand))
            {
                return queryBrand;
            }

            return string.Empty;
        }

        /// <summary>
        /// Extract the brand from the cookies.
        /// </summary>
        /// <param name="context">The filter context from the request.</param>
        /// <returns>The brand code from the cookies or an empty string if nothing can found.</returns>
        private static string FromCookie(ControllerContext context)
        {
            var userCookie = context.RequestContext.HttpContext.Request.Cookies[Parameter];

            if (userCookie != null && IsValid(userCookie.Value))
            {
                return userCookie.Value;
            }

            return string.Empty;
        }

        private static void SaveInClaim(string brandCode)
        {
            var brandClaim = ClaimsPrincipal.Current.FindFirst(PortalClaimTypes.Brand);

            if (brandClaim == null || brandClaim.Value == brandCode)
                // Benutzer ist nicht angemeldet oder die Brand entspricht bereits der gewünschten Einstellung.
                return;

            var sam = FederatedAuthentication.SessionAuthenticationModule;

            SessionSecurityToken token;

            if (sam.TryReadSessionTokenFromCookie(out token))
            {
                var identity = token.ClaimsPrincipal.Identities.First();

                // Entferne existierende Claims falls vorhanden
                var claims = ClaimsPrincipal.Current.FindAll(x => x.Type == PortalClaimTypes.Brand);

                foreach (var c in claims)
                    identity.RemoveClaim(c);

                // Den neuen Claim hinzufügen
                var newClaim = new Claim(PortalClaimTypes.Brand, brandCode);
                identity.AddClaim(newClaim);

                // Generiere das SecurityToken neu und überschreibe das cookie mit dem neuen Token
                var newToken = sam.CreateSessionSecurityToken(token.ClaimsPrincipal, token.Context, token.ValidFrom, token.ValidTo, token.IsPersistent);
                sam.WriteSessionTokenToCookie(newToken);
            }
        }

        /// <summary>
        /// Saves the brand in the cookies if it not.
        /// </summary>
        /// <param name="context">The context from the request.</param>
        /// <param name="brandCode">The brand code to save.</param>
        private static void SaveInCookie(ControllerContext context, string brandCode)
        {
            var cookieValue = FromCookie(context);

            //Save only in the cookie if not set or the value changed.
            if (string.IsNullOrEmpty(cookieValue) || !string.Equals(cookieValue, brandCode, StringComparison.InvariantCultureIgnoreCase))
            {
                HttpContext.Current.Response.Cookies.Set(
                    new HttpCookie(Parameter, brandCode)
                    {
                        HttpOnly = true,
                        Expires = DateTime.Now.AddMonths(1)
                    }
                );
            }
        }

        private static void SaveInViewBag(ControllerContext context, string brandCode)
        {
            context.Controller.ViewBag.Brand = brandCode;
        }

        private static void SaveInThread(string brandCode)
        {
            Current = brandCode;
        }
    }
}