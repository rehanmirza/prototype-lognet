﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace Techgroup.AspNet.Portal.Brand
{
    /// <summary>
    /// This attribute handled the brand.
    /// </summary>
    public sealed class SetBrandAttribute : FilterAttribute, IActionFilter
    {
        /// <summary>
        /// Called after the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context from the request.</param>
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // not needed
        }

        /// <summary>
        /// Called before an action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context from the request.</param>
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var brand = BrandConfig.From(filterContext);

            brand = BrandConfig.NormalizeBrand(brand);

            BrandConfig.Save(filterContext, brand);
        }
    }
}