﻿Portal Brand
==============
Kopiere die Klassen von diesem Ordner in dein Projekt und kopiere den unteren Code in die FilterConfig und Web.Config.

Danach kann auf den Brand via BrandConfig.Current oder ViewBag.Brand zugegriffen werden.

    public static class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //...
            filters.Add(new SetBrandAttribute());
            //...
        }
    }

    <!-- Die AppSetting Brand.Supported erwartet eine mit ; separierte Liste von unterstützen Brands. Der erste Eintrag wird als Default verwendet. -->
    <add key="Brand.Supported" value="planzer;alpincargo;kuoni;marti;polysys;qualinight;roeoesli;ruckstuhl;schoenholzer;senn;tz;wespe;wma" />
    <!-- Die AppSetting Brand.PathToLogo erwartet einen URL mit einem {0} Platzhalter für den Brand. -->
    <add key="Brand.PathToLogo" value="https://testplaportalsto.blob.core.windows.net/files/Images/Logos/{0}.png" />