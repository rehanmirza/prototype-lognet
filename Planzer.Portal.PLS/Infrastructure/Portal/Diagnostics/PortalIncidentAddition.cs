﻿using System;
using System.Linq;
using System.Security.Claims;
using Techgroup.AspNet.Portal.Authentication;
using Techgroup.Diagnostics;

namespace Techgroup.AspNet.Portal.Diagnostics
{
    public static class PortalIncidentAddition
    {
        public static void Fill(Incident incident)
        {
            try
            {
                if (ClaimsPrincipal.Current.HasClaim(x => x.Type == PortalClaimTypes.PortalAccountName))
                {
                    incident.AddTriggerInfo("Account: " + ClaimsPrincipal.Current.FindFirst(PortalClaimTypes.PortalAccountName).Value);
                }

                if (ClaimsPrincipal.Current.HasClaim(x => x.Type == ClaimTypes.Name))
                {
                    incident.AddTriggerInfo("User: " + ClaimsPrincipal.Current.FindFirst(ClaimTypes.Name).Value);
                }
            }
            catch { }
        }
    }
}