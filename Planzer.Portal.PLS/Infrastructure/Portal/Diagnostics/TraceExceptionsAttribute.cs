﻿using System;
using System.Linq;
using System.Web.Mvc;
using Techgroup.AspNet.Diagnostics;
using Techgroup.Diagnostics;

namespace Techgroup.AspNet.Portal.Diagnostics
{
    public class TraceExceptionsAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var incident = new Incident();

            AppSettingsIncidentAddition.Fill(incident);
            PortalIncidentAddition.Fill(incident);
            AssemblyIncidentAddition.Fill(incident);
            ExceptionIncidentAddition.Fill(incident, context.Exception);
            MvcIncidentAddition.Fill(incident, context.HttpContext.Request);

            DiagnosticsLog.Trace(incident);
        }
    }
}