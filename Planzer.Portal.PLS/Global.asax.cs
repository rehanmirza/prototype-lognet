﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Claims;
using System.IdentityModel.Services;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Techgroup.AspNet.Portal.Authentication;

namespace Planzer.Portal.PLS
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_BeginRequest()
        {
            PortalAuthenticationCookie.SetLocalCookieForDebugWithLocalhost(Context);

            RedirectIfNeeded();
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            MvcHandler.DisableMvcResponseHeader = true;
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;

            FederatedAuthentication.FederationConfigurationCreated += PortalAuthenticationCookie.SetConfigForCookieHandler;
        }

        protected void SessionAuthenticationModule_SessionSecurityTokenReceived(object sender, SessionSecurityTokenReceivedEventArgs e)
        {
            PortalAuthenticationCookie.RefreshSecurityToken(sender, e);
        }

        protected void Application_PreSendRequestHeaders(Object source, EventArgs e)
        {
            RemoveUnnecessaryHeaders();
        }

        private void RedirectIfNeeded()
        {
            if (Request.Url.Host.Equals("plaportalplsweb.azurewebsites.net", StringComparison.InvariantCultureIgnoreCase))
            {
                // Redirect from platntweb.azurewebsites.net to productive domain.
                Response.RedirectPermanent($"{Request.Url.Scheme}://pls.planzergroup.com{Request.Url.PathAndQuery}",
                    endResponse: true);
            }
            else if (Request.Url.Host.Equals("testplaportalplsweb.azurewebsites.net", StringComparison.InvariantCultureIgnoreCase))
            {
                // Redirect from testplatntweb.azurewebsites.net to test domain domain.
                Response.RedirectPermanent($"{Request.Url.Scheme}://plstest.planzergroup.com{Request.Url.PathAndQuery}",
                    endResponse: true);
            }
            else if (!Context.Request.IsSecureConnection && !Context.Request.IsLocal
            && string.Equals(Request.Url.Scheme, "http", StringComparison.InvariantCultureIgnoreCase))
            {
                // Permanente Umleitung auf HTTPS. (Ausser bei lokalen Aufrufen mit localhost). Use
                // permanent to prevent Session Hijacking Attacks
                Response.RedirectPermanent($"https://{Request.Url.Host}{Request.Url.PathAndQuery}",
                    endResponse: true);
            }
        }

        /// <summary>
        /// Prevent Information Leakage: Environment information, remove unnecessary headers
        /// </summary>
        private void RemoveUnnecessaryHeaders()
        {
            Response.Headers.Remove("Server");
            Response.Headers.Remove("X-AspNetMvc-Version");
            Response.Headers.Remove("X-AspNet-Version");
        }
    }
 }
