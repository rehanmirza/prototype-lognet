﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Planzer.Portal.PLS.Models;

namespace Planzer.Portal.PLS.Database
{
    [DbConfigurationType(typeof(SqlAzureExecutionStrategyDbConfiguration))]
    public class PLSDb : DbContext
    {
        public PLSDb()
            : base(Config.PlanzerPortalDbConnString)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            Database.CommandTimeout = 3600;
            System.Data.Entity.Database.SetInitializer<PLSDb>(null);
        }

        public DbSet<Lager> Lager { get; set; }
        public DbSet<PIDBestand> PIDBestand { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        }
    }
}