﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;

namespace Planzer.Portal.PLS.Database
{
    /// <summary>
    /// Diese sollte für jede Datenbank welche in Azure gehostet ist verwendet werden, weil sonst
    /// von Zeit zu Zeit eine Exception auftritt. Fix the exception:
    /// System.Data.Entity.Core.EntityException: An exception has been raised that is likely due to
    /// a transient failure...
    /// Source: http://msdn.microsoft.com/en-us/data/dn456835 Sie kann wie folgt auf den DbContext
    /// angewendet werden: [DbConfigurationType(typeof(SqlAzureExecutionStrategyDbConfiguration))]
    /// public partial class MyDbContext : DbContext Sie auch Snippet DbContext
    /// </summary>
    public class SqlAzureExecutionStrategyDbConfiguration : DbConfiguration
    {
        public SqlAzureExecutionStrategyDbConfiguration()
        {
            SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy());
        }
    }
}